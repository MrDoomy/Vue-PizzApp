import { Notification } from '@/components/ui';

export const mixinShortMessage = delay => ({
  components: {
    Notification
  },
  data() {
    return {
      message: '',
      color: ''
    };
  },
  methods: {
    setMessage(message) {
      this.message = message;

      setTimeout(() => {
        this.message = '';
      }, delay);
    },
    colorizeRed() {
      this.color = 'red';
    },
    colorizeGreen() {
      this.color = 'green';
    },
    colorizeBlue() {
      this.color = 'blue';
    },
    colorizeYellow() {
      this.color = 'yellow';
    }
  }
});

export const mixinRedirect = {
  methods: {
    redirect(notAllowed, pathName) {
      if (notAllowed) {
        this.$router.push({ name: pathName });
      }
    }
  }
};
