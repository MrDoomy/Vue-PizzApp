import axios from 'axios';
import api from './api';

/**
 * Fetch Login Account
 *
 * @param {Object} account Account
 * @returns {Promise} Response
 * @throws {Error} Error
 */
export const fetchLoginAccount = account => {
  return axios.post(api.login(), account)
    .then((response => {
      return response.data;
    }))
    .catch(() => {
      throw new Error('Login Account Failure !')
    });
};

/**
 * Fetch Register Account
 *
 * @param {Object} account Account
 * @returns {Promise} Response
 * @throws {Error} Error
 */
export const fetchRegisterAccount = account => {
  return axios.post(api.register(), account)
    .then((response => {
      return response.data;
    }))
    .catch(() => {
      throw new Error('Register Account Failure !')
    });
};

/**
 * Fetch Read Account
 *
 * @param {String} token Token
 * @returns {Promise} Response
 * @throws {Error} Error
 */
export const fetchReadAccount = token => {
  return axios.get(api.account(), {
    headers: {
      'Authorization': `Bearer ${token}`
    }
  })
    .then((response => {
      return response.data;
    }))
    .catch(() => {
      throw new Error('Read Account Failure !')
    });
};

/**
 * Fetch Logout Account
 *
 * @param {String} token Token
 * @returns {Promise} Response
 * @throws {Error} Error
 */
export const fetchLogoutAccount = token => {
  return axios.get(api.logout(), {
    headers: {
      'Authorization': `Bearer ${token}`
    }
  })
    .then((response => {
      return response.data;
    }))
    .catch(() => {
      throw new Error('Logout Account Failure !')
    });
};

/**
 * Fetch Pswd Account
 *
 * @param {String} token Token
 * @param {Object} credentials Credentials
 * @returns {Promise} Response
 * @throws {Error} Error
 */
export const fetchPswdAccount = (token, credentials) => {
  return axios.put(api.pswd(), credentials, {
    headers: {
      'Authorization': `Bearer ${token}`
    }
  })
    .then((response => {
      return response.data;
    }))
    .catch(() => {
      throw new Error('Pswd Account Failure !')
    });
};

/**
 * Fetch Update Account
 *
 * @param {String} token Token
 * @param {Object} account Account
 * @returns {Promise} Response
 * @throws {Error} Error
 */
export const fetchUpdateAccount = (token, account) => {
  return axios.put(api.account(), account, {
    headers: {
      'Authorization': `Bearer ${token}`
    }
  })
    .then((response => {
      return response.data;
    }))
    .catch(() => {
      throw new Error('Update Account Failure !')
    });
};

/**
 * Fetch Delete Account
 *
 * @param {String} token Token
 * @returns {Promise} Response
 * @throws {Error} Error
 */
export const fetchDeleteAccount = token => {
  return axios.delete(api.account(), {
    headers: {
      'Authorization': `Bearer ${token}`
    }
  })
    .then((response => {
      return response.data;
    }))
    .catch(() => {
      throw new Error('Delete Account Failure !')
    });
};
