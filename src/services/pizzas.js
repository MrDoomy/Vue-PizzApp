import axios from 'axios';
import api from './api';

/**
 * Fetch All Pizzas
 *
 * @returns {Promise} Response
 * @throws {Error} Error
 */
export const fetchAllPizzas = () => {
  return axios.get(api.pizzas())
    .then((response => {
      return response.data;
    }))
    .catch(() => []);
};
