import { render, fireEvent } from '@testing-library/vue';
import Field from '../Field';

describe('Field: Component', () => {
  it('Should Component Renders', () => {
    const { queryByPlaceholderText } = render(Field, {
      props: {
        placeholder: 'Test',
        value: 'Lorem Ipsum'
      }
    });

    expect(queryByPlaceholderText('Test')).toBeInTheDocument();
  });

  it('Should Input Trigger Works', async () => {
    const { queryByPlaceholderText, emitted } = render(Field, {
      props: {
        placeholder: 'Test',
        value: 'Lorem Ipsum'
      }
    });

    const input = queryByPlaceholderText('Test');

    await fireEvent.input(input, { target: { value: 'Lorem Ipsum' }});

    expect(emitted()).toHaveProperty('input');
  });
});
