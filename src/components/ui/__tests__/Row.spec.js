import { render } from '@testing-library/vue';
import Row from '../Row';

describe('Row: Component', () => {
  it('Should Component Renders', () => {
    const { getByText } = render(Row, {
      scopedSlots: {
        default: '<span>Lorem Ipsum</span>'
      }
    });

    expect(getByText('Lorem Ipsum')).toBeInTheDocument();
  });
});
