import { render, screen } from '@testing-library/vue';
import Column from '../Column';

describe('Column: Component', () => {
  it('Should Component Renders', () => {
    render(Column, {
      scopedSlots: {
        default: '<span>Lorem Ipsum</span>'
      }
    });

    expect(screen.getByText('Lorem Ipsum')).toBeInTheDocument();
  });
});
