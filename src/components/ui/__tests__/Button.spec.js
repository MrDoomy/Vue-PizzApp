import { render, fireEvent, screen } from '@testing-library/vue';
import Button from '../Button';

describe('Button: Component', () => {
  it('Should Component Renders', () => {
    render(Button, {
      scopedSlots: {
        default: '<span>Lorem Ipsum</span>'
      }
    });

    expect(screen.getByText('Lorem Ipsum')).toBeInTheDocument();
  });

  it('Should Click Trigger Works', async () => {
    const { getByRole, emitted } = render(Button, {
      scopedSlots: {
        default: '<span>Lorem Ipsum</span>'
      }
    });

    const button = getByRole('button');

    await fireEvent.click(button);

    expect(emitted()).toHaveProperty('click');
  });
});
