import Home from './Home';
import Login from './Login';
import NavBar from './NavBar';
import Pizza from './Pizza';
import Register from './Register';
import Settings from './Settings';

export {
  Home,
  Login,
  NavBar,
  Pizza,
  Register,
  Settings
};
