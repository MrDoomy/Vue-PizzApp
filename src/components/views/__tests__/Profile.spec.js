import { fireEvent, waitFor } from '@testing-library/vue';
import { renderWithRouterAndVuex } from '@/testUtils';
import Profile from '../Profile';

describe('Profile: Component', () => {
  it('Should Component Renders', () => {
    const { queryByDisplayValue } = renderWithRouterAndVuex(Profile, {
      state: {
        account: {
          email: 'rick.sanchez@pm.me',
          firstName: 'Rick',
          lastName: 'Sanchez',
          gender: 'M',
          yearOld: 70,
          token: 'ABCDEF123456'
        }
      }
    });

    waitFor(() => {
      expect(queryByDisplayValue('Rick')).toBeInTheDocument();
      expect(queryByDisplayValue('Sanchez')).toBeInTheDocument();
    });
  });

  it('Should Form Validation Displays Error', async () => {
    const { queryByDisplayValue, getByRole, queryByText } = await renderWithRouterAndVuex(Profile, {
      state: {
        account: {
          email: 'rick.sanchez@pm.me',
          firstName: 'Rick',
          lastName: 'Sanchez',
          gender: 'M',
          yearOld: 70,
          token: 'ABCDEF123456'
        }
      }
    });

    const input = queryByDisplayValue('rick.sanchez@pm.me');

    await fireEvent.input(input, { target: { value: '' } });

    const button = getByRole('button');

    await fireEvent.click(button);

    expect(queryByText('Email Error !')).toBeInTheDocument();
  });
});
