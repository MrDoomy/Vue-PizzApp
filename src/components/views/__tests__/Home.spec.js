import { fireEvent } from '@testing-library/vue';
import { renderWithRouterAndVuex } from '@/testUtils';
import Home from '../Home';

describe('Home: Component', () => {
  it('Should Component Renders', () => {
    const { queryByText } = renderWithRouterAndVuex(Home, {
      state: {
        pizzas: [
          {
            id: 'ABCDEF123456',
            label: '4 Cheeses',
            items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
            price: 14.9
          },
          {
            id: 'ABCDEF123457',
            label: 'Atlas',
            items: ['Mozzarella', 'Spicy Chicken', 'Chorizo', 'Marinated Zucchini', 'Ras-El-Hanout Spices'],
            price: 13.6
          },
          {
            id: 'ABCDEF123458',
            label: 'Barcelona',
            items: ['Mozzarella', 'Chorizo', 'Fried Onions', 'Peppers', 'Olives'],
            price: 13.3
          },
          {
            id: 'ABCDEF123459',
            label: 'Basque',
            items: ['Mozzarella', 'Spicy Chicken', 'Peppers', 'Candied Tomatoes'],
            price: 13.6
          }
        ]
      }
    });

    expect(queryByText('4 Cheeses')).toBeInTheDocument();
    expect(queryByText('Atlas')).toBeInTheDocument();
    expect(queryByText('Barcelona')).toBeInTheDocument();
    expect(queryByText('Basque')).toBeInTheDocument();
  });

  it('Should Form Validation Displays Error', async () => {
    const { queryByText, queryByPlaceholderText } = renderWithRouterAndVuex(Home, {
      state: {
        pizzas: [
          {
            id: 'ABCDEF123456',
            label: '4 Cheeses',
            items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
            price: 14.9
          },
          {
            id: 'ABCDEF123457',
            label: 'Atlas',
            items: ['Mozzarella', 'Spicy Chicken', 'Chorizo', 'Marinated Zucchini', 'Ras-El-Hanout Spices'],
            price: 13.6
          },
          {
            id: 'ABCDEF123458',
            label: 'Barcelona',
            items: ['Mozzarella', 'Chorizo', 'Fried Onions', 'Peppers', 'Olives'],
            price: 13.3
          },
          {
            id: 'ABCDEF123459',
            label: 'Basque',
            items: ['Mozzarella', 'Spicy Chicken', 'Peppers', 'Candied Tomatoes'],
            price: 13.6
          }
        ]
      }
    });

    const input = queryByPlaceholderText('Filter');

    await fireEvent.input(input, { target: { value: 'Ba' } });

    expect(queryByText('4 Cheeses')).not.toBeInTheDocument();
    expect(queryByText('Atlas')).not.toBeInTheDocument();
    expect(queryByText('Barcelona')).toBeInTheDocument();
    expect(queryByText('Basque')).toBeInTheDocument();
  });
});
