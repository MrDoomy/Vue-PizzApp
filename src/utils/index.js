/**
 * Is Empty
 *
 * @param {Object} obj Obj
 * @returns {Boolean} Status
 */
export const isEmpty = obj => {
  if (Object.entries(obj).length === 0) {
    return true;
  }

  return false;
};

/**
 * Sort By Key
 *
 * @param {String} key Key 
 */
export const sortByKey = key => (a, b) => {
  return a[key] > b[key] ? 1 : (a[key] < b[key] ? -1 : 0);
};
