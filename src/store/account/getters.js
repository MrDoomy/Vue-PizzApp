import { isEmpty } from '@/utils';

export const getters = {
  getAccount(state) {
    return state.account || {};
  },
  getLogin(_, getters) {
    const account = getters.getAccount;

    if (!isEmpty(account)) {
      return account.login;
    }

    return '';
  },
  getEmail(_, getters) {
    const account = getters.getAccount;

    if (!isEmpty(account)) {
      return account.email;
    }

    return '';
  },
  getFirstName(_, getters) {
    const account = getters.getAccount;

    if (!isEmpty(account)) {
      return account.firstName;
    }

    return '';
  },
  getLastName(_, getters) {
    const account = getters.getAccount;

    if (!isEmpty(account)) {
      return account.lastName;
    }

    return '';
  },
  getGender(_, getters) {
    const account = getters.getAccount;

    if (!isEmpty(account)) {
      return account.gender;
    }

    return '';
  },
  getYearOld(_, getters) {
    const account = getters.getAccount;

    if (!isEmpty(account)) {
      return account.yearOld;
    }

    return '';
  },
  getToken(_, getters) {
    const account = getters.getAccount;

    if (!isEmpty(account)) {
      return account.token;
    }

    return null;
  }
};
