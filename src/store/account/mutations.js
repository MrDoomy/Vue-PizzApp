import { SET_TOKEN, SET_ACCOUNT, RESET_ACCOUNT } from './constants';

export const mutations = {
  [SET_TOKEN](state, payload) {
    state.account = {
      ...state.account,
      token: payload
    };
  },
  [SET_ACCOUNT](state, payload) {
    state.account = {
      ...state.account,
      ...payload
    };
  },
  [RESET_ACCOUNT](state) {
    state.account = {
      login: '',
      email: '',
      firstName: '',
      lastName: '',
      gender: '',
      yearOld: 0,
      token: null
    };
  }
};
