import { actions } from '../actions';

describe('Account: Actions', () => {
  const commit = jest.fn();

  it("Should 'setToken' Calls 'type' & 'payload'", () => {
    actions.setToken({ commit }, 'ABCDEF123456');

    expect(commit).toHaveBeenCalledWith('[Account] Set Token', 'ABCDEF123456');
  });

  it("Should 'setAccount' Calls 'type' & 'payload'", () => {
    const payload = {
      login: 'Pickle',
      email: 'rick.sanchez@pm.me',
      firstName: 'Lorem',
      lastName: 'Ipsum',
      gender: 'M',
      yearOld: 70
    };

    actions.setAccount({ commit }, payload);

    expect(commit).toHaveBeenCalledWith('[Account] Set Account', payload);
  });

  it("Should 'resetAccount' Calls 'type'", () => {
    actions.resetAccount({ commit });

    expect(commit).toHaveBeenCalledWith('[Account] Reset Account');
  });
});
