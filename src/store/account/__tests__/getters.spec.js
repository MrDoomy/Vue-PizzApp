import { getters } from '../getters';

describe('Account: Getters', () => {
  const state = {
    account: {
      login: 'Pickle',
      email: 'rick.sanchez@pm.me',
      firstName: 'Rick',
      lastName: 'Sanchez',
      gender: 'M',
      yearOld: 70,
      token: 'ABCDEF123456'
    }
  };

  it("Should 'getAccount' Returns State", () => {
    expect(getters.getAccount({})).toEqual({});
    expect(getters.getAccount(state)).toEqual({
      login: 'Pickle',
      email: 'rick.sanchez@pm.me',
      firstName: 'Rick',
      lastName: 'Sanchez',
      gender: 'M',
      yearOld: 70,
      token: 'ABCDEF123456'
    });
  });

  it("Should 'getLogin' Returns 'login'", () => {
    expect(getters.getLogin({}, getters)).toBeFalsy();
    expect(getters.getLogin(state, {
      ...getters,
      getAccount: state.account
    })).toEqual('Pickle');
  });

  it("Should 'getEmail' Returns 'email'", () => {
    expect(getters.getEmail({}, getters)).toBeFalsy();
    expect(getters.getEmail(state, {
      ...getters,
      getAccount: state.account
    })).toEqual('rick.sanchez@pm.me');
  });

  it("Should 'getFirstName' Returns 'firstName'", () => {
    expect(getters.getFirstName({}, getters)).toBeFalsy();
    expect(getters.getFirstName(state, {
      ...getters,
      getAccount: state.account
    })).toEqual('Rick');
  });

  it("Should 'getLastName' Returns 'lastName'", () => {
    expect(getters.getLastName({}, getters)).toBeFalsy();
    expect(getters.getLastName(state, {
      ...getters,
      getAccount: state.account
    })).toEqual('Sanchez');
  });

  it("Should 'getGender' Returns 'gender'", () => {
    expect(getters.getGender({}, getters)).toBeFalsy();
    expect(getters.getGender(state, {
      ...getters,
      getAccount: state.account
    })).toEqual('M');
  });

  it("Should 'getYearOld' Returns 'yearOld'", () => {
    expect(getters.getYearOld({}, getters)).toBeFalsy();
    expect(getters.getYearOld(state, {
      ...getters,
      getAccount: state.account
    })).toEqual(70);
  });

  it("Should 'getToken' Returns 'token'", () => {
    expect(getters.getToken({}, getters)).toBeFalsy();
    expect(getters.getToken(state, {
      ...getters,
      getAccount: state.account
    })).toEqual('ABCDEF123456');
  });
});
