import { mutations } from '../mutations';

describe('Account: Mutations', () => {
  const initialState = {
    login: '',
    email: '',
    firstName: '',
    lastName: '',
    gender: '',
    yearOld: 0,
    token: null
  };

  it("Should 'Set Token' Case Returns State", () => {
    const state = {
      account: {
        ...initialState
      }
    };

    mutations['[Account] Set Token'](state, 'ABCDEF123456');

    expect(state.account).toEqual({
      ...initialState,
      token: 'ABCDEF123456'
    });
  });

  it("Should 'Set Account' Case Returns State", () => {
    const state = {
      account: {
        ...initialState
      }
    };

    const payload = {
      login: 'Pickle',
      email: 'rick.sanchez@pm.me',
      firstName: 'Lorem',
      lastName: 'Ipsum',
      gender: 'M',
      yearOld: 70
    };

    mutations['[Account] Set Account'](state, payload);

    expect(state.account).toEqual({
      ...initialState,
      ...payload
    });
  });

  it("Should 'Reset Account' Case Returns Initial State", () => {
    const state = {
      account: {
        login: 'Pickle',
        email: 'rick.sanchez@pm.me',
        firstName: 'Lorem',
        lastName: 'Ipsum',
        gender: 'M',
        yearOld: 70,
        token: 'ABCDEF123456'
      }
    };

    mutations['[Account] Reset Account'](state);

    expect(state.account).toEqual(initialState);
  });
});
