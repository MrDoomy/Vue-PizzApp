export * from './actions';
export * from './getters';
export * from './mutations';

export const initialState = {
  login: '',
  email: '',
  firstName: '',
  lastName: '',
  gender: '',
  yearOld: 0,
  token: null
};
