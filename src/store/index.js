import Vue from 'vue';
import Vuex, { Store } from 'vuex';
import {
  initialState as account,
  actions as accountActions,
  getters as accountGetters,
  mutations as accountMutations,
} from './account';
import * as Pizzas from './pizzas'; 

Vue.use(Vuex);

/**
 * OPTIONAL: Set Store Item (In Session Store)
 */
const storagePlugin = store => {
  store.subscribe((_, state) => {
    sessionStorage.setItem('store', JSON.stringify(state));
  });
};

// OPTIONAL: Get Store Item (From Session Storage) 
const storeItem = sessionStorage.getItem('store');

const persistedState = storeItem ? JSON.parse(storeItem) : {
  account,
  pizzas: Pizzas.initialState
};

export const store = {
  state: persistedState,
  actions: {
    ...accountActions,
    ...Pizzas.actions
  },
  getters: {
    ...accountGetters,
    ...Pizzas.getters
  },
  mutations: {
    ...accountMutations,
    ...Pizzas.mutations
  },
  plugins: [storagePlugin]
};

export default new Store(store);
