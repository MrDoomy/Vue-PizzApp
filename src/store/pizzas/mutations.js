import { SET_PIZZAS, ADD_PIZZA, UP_PIZZA, DEL_PIZZA, RESET_PIZZAS } from './constants';

export const mutations = {
  [SET_PIZZAS](state, payload) {
    state.pizzas = payload;
  },
  [ADD_PIZZA](state, payload) {
    state.pizzas = [
      ...state.pizzas,
      payload
    ];
  },
  [UP_PIZZA](state, payload) {
    state.pizzas = state.pizzas.map(pizza => pizza.id === payload.id ? payload : pizza);
  },
  [DEL_PIZZA](state, payload) {
    state.pizzas = state.pizzas.filter(pizza => pizza.id !== payload);
  },
  [RESET_PIZZAS](state) {
    state.pizzas = [];
  }
};
