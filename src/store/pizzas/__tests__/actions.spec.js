import { actions } from '../actions';

describe('Pizzas: Actions', () => {
  const commit = jest.fn();

  it("Should 'setPizzas' Calls 'type' & 'payload'", () => {
    const payload = [
      {
        id: 'ABCDEF123456',
        label: '4 Cheeses',
        items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      }
    ];

    actions.setPizzas({ commit }, payload);

    expect(commit).toHaveBeenCalledWith('[Pizzas] Set Pizzas', payload);
  });

  it("Should 'addPizza' Returns 'type' & 'payload'", () => {
    const payload = {
      id: 'ABCDEF123456',
      label: '4 Cheeses',
      items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
      price: 14.9
    };

    actions.addPizza({ commit }, payload);

    expect(commit).toHaveBeenCalledWith('[Pizzas] Add Pizza', payload);
  });

  it("Should 'upPizza' Returns 'type' & 'payload'", () => {
    const payload = {
      id: 'ABCDEF123456',
      label: '3 Cheeses',
      items: ['Mozzarella', 'Reblochon', 'Gorgonzola'],
      price: 9.9
    };

    actions.upPizza({ commit }, payload);

    expect(commit).toHaveBeenCalledWith('[Pizzas] Up Pizza', payload);
  });

  it("Should 'delPizza' Returns 'type' & 'payload'", () => {
    actions.delPizza({ commit }, 'ABCDEF123456');

    expect(commit).toHaveBeenCalledWith('[Pizzas] Del Pizza', 'ABCDEF123456');
  });

  it("Should 'resetPizzas' Returns 'type'", () => {
    actions.resetPizzas({ commit });

    expect(commit).toHaveBeenCalledWith('[Pizzas] Reset Pizzas');
  });
});
