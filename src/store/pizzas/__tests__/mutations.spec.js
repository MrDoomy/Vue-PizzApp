import { mutations } from '../mutations';

describe('Pizzas: Mutations', () => {
  const initialState = [];

  it("Should 'Set Pizzas' Case Returns State", () => {
    const state = {
      pizzas: [
        ...initialState
      ]
    };

    const payload = [
      {
        id: 'ABCDEF123456',
        label: '4 Cheeses',
        items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      }
    ];

    mutations['[Pizzas] Set Pizzas'](state, payload);

    expect(state.pizzas).toEqual(payload);
  });

  it("Should 'Add Pizza' Case Returns State", () => {
    const state = {
      pizzas: [
        ...initialState
      ]
    };

    const payload = {
      id: 'ABCDEF123456',
      label: '4 Cheeses',
      items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
      price: 14.9
    };

    mutations['[Pizzas] Add Pizza'](state, payload);

    expect(state.pizzas).toEqual([
      ...initialState,
      payload
    ]);
  });

  it("Should 'Up Pizza' Case Returns State", () => {
    const state = {
      pizzas: [
        ...initialState,
        {
          id: 'ABCDEF123456',
          label: '4 Cheeses',
          items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        }
      ]
    };

    const payload = {
      id: 'ABCDEF123456',
      label: '3 Cheeses',
      items: ['Mozzarella', 'Reblochon', 'Gorgonzola'],
      price: 9.9
    };

    mutations['[Pizzas] Up Pizza'](state, payload);

    expect(state.pizzas).toEqual(
      state.pizzas.map(val => val.id === 'ABCDEF123456' ? payload : val)
    );
  });

  it("Should 'Del Pizza' Case Returns State", () => {
    const state = {
      pizzas: [
        ...initialState,
        {
          id: 'ABCDEF123456',
          label: '4 Cheeses',
          items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        }
      ]
    };

    mutations['[Pizzas] Del Pizza'](state, 'ABCDEF123456');

    expect(state.pizzas).toEqual(
      state.pizzas.filter(val => val.id !== 'ABCDEF123456')
    );
  });

  it("Should 'Reset Pizzas' Case Returns Initial State", () => {
    const state = {
      pizzas: [
        {
          id: 'ABCDEF123456',
          label: '4 Cheeses',
          items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        }
      ]
    };

    mutations['[Pizzas] Reset Pizzas'](state);

    expect(state.pizzas).toEqual(initialState);
  });
});
