import { getters } from '../getters';

describe('Pizzas: Getters', () => {
  const state = {
    pizzas: [
      {
        id: 'ABCDEF123456',
        label: '4 Cheeses',
        items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      }
    ]
  };

  it("Should 'getPizzas' Returns State", () => {
    expect(getters.getPizzas({})).toHaveLength(0);
    expect(getters.getPizzas(state)).toHaveLength(1);
  });

  it("Should 'getPizzaById' Returns 'pizza'", () => {
    expect(getters.getPizzaById(state, {
      ...getters,
      getPizzas: state.pizzas
    })('ABCDEF123456')).toEqual({
      id: 'ABCDEF123456',
      label: '4 Cheeses',
      items: ['Mozzarella', 'Goat Cheese', 'Reblochon', 'Gorgonzola'],
      price: 14.9
    });
  });
});
