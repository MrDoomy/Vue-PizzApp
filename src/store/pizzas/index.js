export * from './actions';
export * from './getters';
export * from './mutations';

export const initialState = []