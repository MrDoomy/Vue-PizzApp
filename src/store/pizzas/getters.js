export const getters = {
  getPizzas(state) {
    return state.pizzas || [];
  },
  getPizzaById(_, getters) {
    return id => {
      return getters.getPizzas.find(pizza => pizza.id === id);
    }
  }
};
