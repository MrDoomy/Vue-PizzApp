module.exports = {
  lintOnSave: true,
  devServer: {
    port: 1234,
    proxy: {
      '/api': {
        target: 'http://127.0.0.1:5050'
      }
    }
  }
};
