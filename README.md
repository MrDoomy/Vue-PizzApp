# PizzApp

> Version 0.9.0

## Intro

WebApp based on pizza management to learn the **Vue** framework.

This _frontend_ application was built using following technologies,

- Bulma
- Vue

### Context

- [x] Handling Data & Props
- [x] Handling Form & Events
- [x] Async Call (**Mixins**)
- [x] Vue Router
- [x] Dumb Vs Smart Components
- [x] Vuex
- [x] Side Effects
- [x] _Keep Calm And Coding_
- [x] _All U Need Is Pizza_
- [x] Jest
- [ ] TypeScript

## Process

Repository:

```
git clone -b 0.9.0 https://gitlab.com/dmnchzl/vue-pizzapp.git
```

Install:

```
npm install
```

Launch:

```
npm run serve
```

Build:

```
npm run build
npx serve -s build
```

Test:

```
npm run test:unit
```

## Docs

- **Bulma**: A Free, _Open-Source_ CSS Framework (Based On **Flexbox**)
  - [https://bulma.io/](https://bulma.io/)

- **Vue**: The Progressive JavaScript Framework
  - [https://vuejs.org/](https://vuejs.org/)

- **Vue Router**: The Official Router For Vue
  - [https://router.vuejs.org/](https://router.vuejs.org/)

- **Vuex**: State Management Pattern & Library For Vue Apps
  - [https://router.vuejs.org/](https://router.vuejs.org/)

- **Jest**: A Delightful JavaScript Testing Framework
  - [https://jestjs.io/](https://jestjs.io/)

- **Testing Lib**: Simple & Complete Testing Utilities
  - [https://testing-library.com/](https://testing-library.com/)

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
